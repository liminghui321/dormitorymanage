package com.huawei.service;

import com.huawei.vo.Manager;

import java.util.List;

public interface IManagerService {
    List<Manager> findAllManager();
}
