package com.huawei.service;

import com.huawei.vo.Dormitory;
import com.huawei.vo.Repair;

import java.util.List;

public interface IRepairsService {
    List<Repair> findAllRepairs();
    Repair findRepairById(Integer rid);
    void delRepairById(Integer rid);
    void updateRepair(Repair repair);
    void stuUpdateRepair(Repair repair);
    void addRepair(Repair repair);
    List<Dormitory> findRepairByDid(Integer did);
    List<Dormitory> adminFindRepairByDid();

}
