package com.huawei.service;

import com.huawei.vo.Notice;

import java.util.List;

public interface INoticeService {
    List<Notice> findAllNotices();
    Notice findNoticeById(Integer tid);
    void delNoticeById(Integer tid);
    void updateNotice(Notice notice);
    void addNotice(Notice notice);
}
