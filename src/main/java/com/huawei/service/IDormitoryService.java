package com.huawei.service;

import com.huawei.vo.Dormitory;

import java.util.List;

public interface IDormitoryService {
    List<Dormitory> findAllDormitories();
    List<Dormitory> findAllStudentsByDormitory(Integer did);
    List<Dormitory> adminFindAllStudentsByDormitory();
}
