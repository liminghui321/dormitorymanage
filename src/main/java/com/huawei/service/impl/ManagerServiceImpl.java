package com.huawei.service.impl;

import com.huawei.mapper.ManagerMapper;
import com.huawei.vo.Manager;
import com.huawei.service.IManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("managerService")
public class ManagerServiceImpl implements IManagerService{
    @Autowired
    private ManagerMapper manager;
    @Override
    public List<Manager> findAllManager() {
        return manager.findAllManager();
    }
}
