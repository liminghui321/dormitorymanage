package com.huawei.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MyMvcConfig implements WebMvcConfigurer {
    private static final Logger LOGGER = LoggerFactory.getLogger(MyMvcConfig.class);

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        //浏览器发送"/"，"/index.html"请求时，来到"index"页面
        registry.addViewController("/").setViewName("index");
        registry.addViewController("/index.html").setViewName("index");
        registry.addViewController("/main.html").setViewName("list");
        LOGGER.info("MyMvcConfig-->addViewControllers 请求/或/index.html 跳转到首页");
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new LoginHandlerInterceptor())
                .addPathPatterns("/**").excludePathPatterns("/index.html", "/", "/admin/login", "/css/*", "/js/*", "/img/*");
    }
}
